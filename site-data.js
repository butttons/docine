export default {
	navbar: {
		logo: 'Docine',
		tagline: 'Your health, our priority'
	},
	footer: {
		text: 'For more information contact us at',
		contact: {
			phone: '99999 99999',
			email: 'email@domain.com'
		}
	}
};
