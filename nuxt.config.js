module.exports = {
	head: {
		title: 'Home',
		titleTemplate: '%s - Docine',
		link: [
			{
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.png'
			},
			{
				rel: 'stylesheet',
				href: 'https://fonts.googleapis.com/css?family=Montserrat:400,700|Muli:400,700'
			},
			{ rel: 'stylesheet', href: 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css' }
		]
	},
	loading: {
		color: '#A5D6A7',
		height: '5px'
	}
};
