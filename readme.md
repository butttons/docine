# [Demo](https://docine.herokuapp.com/)

# Made using [nuxt](https://nuxtjs.org/)

# Prerequisites
 - [Node.js](https://nodejs.org/en/download/)

# How to install
 - Clone this repo or download the zip file. Extract in a folder.
 - Open folder in terminal. How to do it on  
 -- [Windows](https://www.howtogeek.com/howto/windows-vista/stupid-geek-tricks-open-an-explorer-window-from-the-command-prompts-current-directory/)
 -- [Mac](https://lifehacker.com/launch-an-os-x-terminal-window-from-a-specific-folder-1466745514)
 - Ensure you are in the right folder by typing `ls` and make sure the list of folders match the contents of the zip file. Folder names include `pages`, `assets`, `components`, etc.
 - Run this command to install: `npm install`

# How to edit and view website locally
Make sure you have installed the files correctly. There should be a `node_modules` folder in our directory now.
 - Run this command to run the server: `npm run dev`
 - Open http://localhost:3000/ to view website.
 - You can edit files and can see the result directly without refreshing the page. When you're done, just close the terminal.

# Website structure.
Nuxt has very [extensive documentation](https://nuxtjs.org/guide/directory-structure) about the file structure used.
In a nutshell - 
 - `layouts` folder has the page layout which all 3 pages use. 
 - `components` folder have the 2 components which make up the majority of the layout, the header and footer.
 - `pages` folder has 3 pages, home, doctors', and patients'. 

### Changing header and footer details
Moved the data to `site-data.js` in root of our directory. It's a simple [javascript object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer).

### Changing data in any of the pages
Using [pug template language](https://pugjs.org/api/getting-started.html) to write the HTML.
The `pages` directory has 3 pages. 
The 3 pages are [Vue single file components](https://vuejs.org/v2/guide/single-file-components.html). More info about how those work on [here](https://nuxtjs.org/guide/views).